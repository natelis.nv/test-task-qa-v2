import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import Firebase, { FirebaseContext } from './components/Firebase';
import axios from 'axios';
import App from './components/App';

axios.defaults.baseURL = 'https://5f92d946eca67c001640a0cc.mockapi.io';

ReactDOM.render(
    <FirebaseContext.Provider value={new Firebase()}>
        <App />
    </FirebaseContext.Provider>,
    document.getElementById('root'));

serviceWorker.unregister();
